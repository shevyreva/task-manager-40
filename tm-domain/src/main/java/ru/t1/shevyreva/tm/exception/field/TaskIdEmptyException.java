package ru.t1.shevyreva.tm.exception.field;

public final class TaskIdEmptyException extends AbsrtactFieldException {

    public TaskIdEmptyException() {
        super("Error! Task id is empty!");
    }

}
