package ru.t1.shevyreva.tm.api.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.enumerated.TaskSort;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm_task (id, name, description, status, created, project_id) " +
            "VALUES (#{id},#{name},#{description},#{status},#{created},#{projectId});")
    void add(@NotNull Task task);

    @Insert("INSERT INTO tm_task (id, user_id, name, description, status, created, project_id) " +
            "VALUES (#{id},#{userId},#{name},#{description},#{status},#{created},#{projectId});")
    void addWithUserId(@NotNull @Param("userId") String userId, @NotNull Task task);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_Id")
    })
    @Nullable
    List<Task> findAllWithUser(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    List<Task> findAll();

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_Id")
    })
    @Nullable
    List<Task> findAllByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_Id")
    })
    @Nullable
    List<Task> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_Id")
    })
    @Nullable
    List<Task> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_Id")
    })
    @Nullable
    Task findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_Id")
    })
   @Nullable
    Task findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT count(*) FROM tm_task WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Update("UPDATE tm_task SET user_id = #{userId}, project_id = #{projectId}, name = #{name}, description = #{description}," +
            " status =#{status} " +
            "WHERE id = #{id}")
    void update(@NotNull final Task task);

    @Delete("DELETE FROM tm_task WHERE id = #{id} AND user_id = #{userId}")
    void removeOne(@NotNull Task model);

    @Delete("DELETE FROM tm_task WHERE id = #{id} AND user_id = #{userId}")
    void removeOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("TRUNCATE TABLE tm_task;")
    void removeAll();

    @Select("SELECT COUNT(1) = 1 FROM tm_task WHERE id = #{id} AND user_id = #{userId}")
    boolean existsById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

}
