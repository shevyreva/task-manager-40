package ru.t1.shevyreva.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.enumerated.ProjectSort;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

public interface IProjectRepository{

    @Insert("INSERT INTO tm_project (id, name, description, status, created) " +
            "VALUES (#{id},#{name},#{description},#{status},#{created});")
    void add(@NotNull Project project);

    @Insert("INSERT INTO tm_project (id, user_id, name, description, status, created " +
            "VALUES (#{id},#{userId},#{name},#{description},#{status},#{created};")
    void addWithUserId(@NotNull @Param("userId") String userId, @NotNull Project project);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId};")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAllWithUser(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAll();

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    Project findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    Project findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT count(*) FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    int getSize(@NotNull @Param("userId") String userId);

    @Update("UPDATE tm_project SET user_id = #{userId}, name = #{name}, description = #{description}," +
            " status =#{status} " +
            "WHERE id = #{id}")
    void update(@NotNull final Project project);

    @Delete("DELETE FROM tm_project WHERE id = #{id} AND user_id = #{userId}")
    void removeOne(@NotNull Project project);

    @Delete("DELETE FROM tm_project WHERE id = #{id} AND user_id = #{userId}")
    void removeOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("TRUNCATE TABLE tm_project;")
    void removeAll();

    @Select("SELECT COUNT(1) = 1 FROM tm_project WHERE id = #{id} AND user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    boolean existsById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

}
